/*
 * 8255_1.c
 *
 * Created: 2017-03-16 오전 10:37:07
 * Author : 9274a
 */ 

#include <avr/io.h>
#include <avr/delay.h>

void write(unsigned char port, unsigned char data){
	PORTC = port;
	PORTB = data;
	_delay_us(50);
	PORTC = 0x04;
	PORTC = 0x0c;
}

int main(void){
	const unsigned char numbers[11]={0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xD8, 0x80, 0x90};
	DDRC = DDRB = 0xff;
	PORTC = 0x0c;
	_delay_us(50);
	PORTC |= 0x10;
	PORTC &= 0xef;
	
	write(0x03, 0x80);
	while(1){
		for(int i = 0;i<10;i++){
			write(0x00, numbers[i]);
			for(int j = 0;j<10;j++){
				write(0x01, numbers[j]);
				for(int k = 0;k<8;k++){
					write(0x02, 0x80 >> k);
					_delay_ms(100);
				}
			}
		}
	}
}

